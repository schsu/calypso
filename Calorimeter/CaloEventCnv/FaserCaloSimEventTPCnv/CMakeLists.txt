###############################################################################
# Package: FaserCaloSimEventTPCnv
################################################################################

# Declare the package name:
atlas_subdir( FaserCaloSimEventTPCnv )

# External dependencies:
find_package( CLHEP )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_library( FaserCaloSimEventTPCnv
                   src/CaloHits/*.cxx
                   PUBLIC_HEADERS FaserCaloSimEventTPCnv
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                   PRIVATE_DEFINITIONS ${CLHEP_DEFINITIONS}
                   LINK_LIBRARIES GaudiKernel GeneratorObjectsTPCnv FaserCaloSimEvent AthenaPoolCnvSvcLib StoreGateLib SGtests
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} TestTools Identifier )

atlas_add_dictionary( FaserCaloSimEventTPCnvDict
                      FaserCaloSimEventTPCnv/CaloSimEventTPCnvDict.h
                      FaserCaloSimEventTPCnv/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} AthenaPoolCnvSvcLib GaudiKernel GeneratorObjectsTPCnv FaserCaloSimEvent TestTools StoreGateLib SGtests Identifier FaserCaloSimEventTPCnv AthenaKernel)

