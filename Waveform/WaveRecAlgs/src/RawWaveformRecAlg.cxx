#include "RawWaveformRecAlg.h"

RawWaveformRecAlg::RawWaveformRecAlg(const std::string& name, 
					 ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator) { 

}

StatusCode 
RawWaveformRecAlg::initialize() {
  ATH_MSG_INFO(name() << "::initalize()" );

  // Initalize tools
  ATH_CHECK( m_recoTool.retrieve() );

  // Set key to read waveform from
  ATH_CHECK( m_waveformContainerKey.initialize() );

  // Set key to read clock info
  ATH_CHECK( m_clockKey.initialize() );

  // Set key to write container
  ATH_CHECK( m_waveformHitContainerKey.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode 
RawWaveformRecAlg::finalize() {
  ATH_MSG_INFO(name() << "::finalize()");

  return StatusCode::SUCCESS;
}

StatusCode 
RawWaveformRecAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("Executing");

  ATH_MSG_DEBUG("Run: " << ctx.eventID().run_number() 
		<< " Event: " << ctx.eventID().event_number());

  // Find the input waveform container
  SG::ReadHandle<RawWaveformContainer> waveformHandle(m_waveformContainerKey, ctx);

  ATH_CHECK( waveformHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for RawWaveformContainer " << m_waveformContainerKey);

  if (waveformHandle->size() == 0) {
    ATH_MSG_DEBUG("Waveform container found with zero length!");
    return StatusCode::SUCCESS;
  }

  // Also find the clock information
  SG::ReadHandle<xAOD::WaveformClock> clockHandle(m_clockKey, ctx);
  const xAOD::WaveformClock* clockptr = NULL;

  // Can survive without this, but make a note
  if ( clockHandle.isValid() ) {
    ATH_MSG_DEBUG("Found ReadHandle for WaveformClock");
    clockptr = clockHandle.ptr();
  } else {
    ATH_MSG_WARNING("Didn't find ReadHandle for WaveformClock!");
  }

  // Find the output waveform container
  SG::WriteHandle<xAOD::WaveformHitContainer> hitContainerHandle(m_waveformHitContainerKey, ctx);
  ATH_CHECK( hitContainerHandle.record( std::make_unique<xAOD::WaveformHitContainer>(),
					std::make_unique<xAOD::WaveformHitAuxContainer>() ) );

  ATH_MSG_DEBUG("WaveformsHitContainer '" << hitContainerHandle.name() << "' initialized");

  // Reconstruct all waveforms
  CHECK( m_recoTool->reconstructAll(*waveformHandle, clockptr, hitContainerHandle.ptr()) );

  ATH_MSG_DEBUG("WaveformsHitContainer '" << hitContainerHandle.name() << "' filled with "<< hitContainerHandle->size() <<" items");

  return StatusCode::SUCCESS;
}

