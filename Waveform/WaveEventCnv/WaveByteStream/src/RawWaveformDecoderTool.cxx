/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#include "RawWaveformDecoderTool.h"
#include "AthenaBaseComps/AthAlgTool.h"

#include "WaveRawEvent/RawWaveform.h"
#include "EventFormats/DigitizerDataFragment.hpp"

static const InterfaceID IID_IRawWaveformDecoderTool("RawWaveformDecoderTool", 1, 0);

const InterfaceID& RawWaveformDecoderTool::interfaceID() {
  return IID_IRawWaveformDecoderTool;
}

RawWaveformDecoderTool::RawWaveformDecoderTool(const std::string& type, 
      const std::string& name,const IInterface* parent)
  : AthAlgTool(type, name, parent)
{
  declareInterface<RawWaveformDecoderTool>(this);

  // These must be configured by job options
  declareProperty("CaloChannels", m_caloChannels);
  declareProperty("VetoChannels", m_vetoChannels);
  declareProperty("TriggerChannels", m_triggerChannels);
  declareProperty("PreshowerChannels", m_preshowerChannels);

  // Test channels is provided for conveniene, not normally used
  declareProperty("TestChannels", m_testChannels);

  // Clock should always be in channel 15
  declareProperty("ClockChannels", m_clockChannels);
  m_clockChannels.push_back(15);

}

RawWaveformDecoderTool::~RawWaveformDecoderTool()
{
}

StatusCode
RawWaveformDecoderTool::initialize() 
{
  ATH_MSG_DEBUG("RawWaveformDecoderTool::initialize()");

  // Set up helpers
  ATH_CHECK(detStore()->retrieve(m_ecalID, "EcalID"));
  ATH_CHECK(detStore()->retrieve(m_vetoID, "VetoID"));
  ATH_CHECK(detStore()->retrieve(m_triggerID, "TriggerID"));
  ATH_CHECK(detStore()->retrieve(m_preshowerID, "PreshowerID"));

  // Take each channel list and create identifiers

  // Loop through digitizer channel lists creating Identifiers
  m_identifierMap.clear();

  // First, calorimeter.  Can either be 4 or 6 channels
  // Bottom row first from left to right, then top row
  int index=0;
  int module=0;
  int row=0;
  int pmt=0;

  int max_modules = m_caloChannels.size() / 2;
  for (auto const& chan : m_caloChannels) {
    row = index / max_modules;
    module = index % max_modules;
    index++;
    // Only store in map if digitizer channel is valid
    if (chan < 0) continue;
    m_identifierMap[chan] = m_ecalID->pmt_id(row, module, pmt);
    ATH_MSG_DEBUG("Mapped digitizer channel " << chan << " to calo ID: " << m_identifierMap[chan]);
  }

  // Next, veto detector.  Have station and plate here.
  int station=0;
  int plate=0;
  pmt=0; 
  index=0;

  int max_stations=m_vetoChannels.size() / 2;
  for (auto const& chan : m_vetoChannels) {
    station = index / max_stations;
    plate = index % max_stations;
    index++;
    // Only store in map if digitizer channel is valid
    if (chan < 0) continue;
    m_identifierMap[chan] = m_vetoID->pmt_id(station, plate, pmt);
    ATH_MSG_DEBUG("Mapped digitizer channel " << chan << " to veto ID: " << m_identifierMap[chan]);
  }

  // Next, trigger detector.  Have pmt and plate.
  pmt=0;
  station=0;
  index=0;
  int max_plates=m_triggerChannels.size() / 2;
  for (auto const& chan : m_triggerChannels) {
    plate = index / max_plates;
    pmt = index % max_plates;
    index++;
    // Only store in map if digitizer channel is valid
    if (chan < 0) continue;
    m_identifierMap[chan] = m_triggerID->pmt_id(station, plate, pmt);
    ATH_MSG_DEBUG("Mapped dititizer channel " << chan << " to trigger ID: " << m_identifierMap[chan]);
  }

  // Finally, preshower detector.
  pmt=0;
  station=0;
  plate=0;
  index=0;
  for (auto const& chan : m_preshowerChannels) {
    plate = index;
    index++;
    // Only store in map if digitizer channel is valid
    if (chan < 0) continue;
    m_identifierMap[chan] = m_preshowerID->pmt_id(station, plate, pmt);
    ATH_MSG_DEBUG("Mapped digitizer channel " << chan << " to preshower ID: " << m_identifierMap[chan]);
  }

  return StatusCode::SUCCESS;
}

StatusCode
RawWaveformDecoderTool::finalize() 
{
  ATH_MSG_DEBUG("RawWaveformDecoderTool::finalize()");
  return StatusCode::SUCCESS;
}

StatusCode
RawWaveformDecoderTool::convert(const DAQFormats::EventFull* re, 
				  RawWaveformContainer* container,
				  const std::string key)
{
  ATH_MSG_DEBUG("RawWaveformDecoderTool::convert("+key+")");

  if (!re) {
    ATH_MSG_ERROR("EventFull passed to convert() is null!");
    return StatusCode::FAILURE;
  }

  if (!container) {
    ATH_MSG_ERROR("RawWaveformContainer passed to convert() is null!");
    return StatusCode::FAILURE;
  }

  // Find the Waveform fragment
  const DigitizerDataFragment* digitizer = NULL;
  const DAQFormats::EventFragment* frag = NULL;
  for(const auto &id : re->getFragmentIDs()) {
    frag=re->find_fragment(id);

    if ((frag->source_id()&0xFFFF0000) != DAQFormats::SourceIDs::PMTSourceID) continue;
    ATH_MSG_DEBUG("Fragment:\n" << *frag);

    digitizer = new DigitizerDataFragment(frag->payload<const uint32_t*>(), frag->payload_size()); 

    break;
  }

  if (!digitizer) {
    ATH_MSG_WARNING("Failed to find digitizer fragment in raw event!");
    return StatusCode::SUCCESS;
  }

  // Check validity here, try to continue, as perhaps not all channels are bad
  if (!digitizer->valid()) {
    ATH_MSG_WARNING("Found invalid digitizer fragment:\n" << *digitizer);
  } else {
    ATH_MSG_DEBUG("Found valid digitizer fragment");
  }

  std::vector<int>* channelList;

  if (key == std::string("CaloWaveforms")) {
    channelList = &m_caloChannels;
  } else if (key == std::string("VetoWaveforms")) {
    channelList = &m_vetoChannels;
  } else if (key == std::string("TriggerWaveforms")) {
    channelList = &m_triggerChannels;
  } else if (key == std::string("PreshowerWaveforms")) {
    channelList = &m_preshowerChannels;
  } else if (key == std::string("TestWaveforms")) {
    channelList = &m_testChannels;
  } else if (key == std::string("ClockWaveforms")) {
    channelList = &m_clockChannels;
  } else {
    ATH_MSG_ERROR("Unknown key " << key);
    return StatusCode::FAILURE;
  }

  for (int channel: *channelList) {
    ATH_MSG_DEBUG("Converting channel "+std::to_string(channel)+" for "+key);

    // Check if this has data
    if (!digitizer->channel_has_data(channel)) {
      ATH_MSG_DEBUG("Channel " << channel << " has no data - skipping!");
      continue;
    } 

    RawWaveform* wfm = new RawWaveform();

    try {
      wfm->setWaveform( channel, digitizer->channel_adc_counts( channel ) );
    } catch ( DigitizerData::DigitizerDataException& e ) {
      ATH_MSG_WARNING("RawWaveformDecoderTool:\n"
		   <<e.what()
		   << "\nChannel "
		   << channel
		   << " not filled!\n");
    }

    try {
      wfm->setHeader( digitizer );

    } catch ( DigitizerData::DigitizerDataException& e ) {
      ATH_MSG_WARNING("RawWaveformDecoderTool:\n"
		      << e.what()
		      << "\nCorrupted Digitizer data!\n"
		      << *frag);
    }

    // Set ID if one exists (clock, for instance, doesn't have an identifier)
    if (m_identifierMap.count(channel) == 1) {
      wfm->setIdentifier(m_identifierMap[channel]);
    }

    container->push_back(wfm);    

    // Sanity check
    if (wfm->adc_counts().size() != wfm->n_samples()) {
      ATH_MSG_WARNING("Created waveform channel" << channel << "with length " << wfm->adc_counts().size() << " but header reports n_samples = " << wfm->n_samples() << "!");
      ATH_MSG_WARNING(*wfm);
    }

  }

  // Don't spring a leak
  delete digitizer;

  ATH_MSG_DEBUG( "RawWaveformDecoderTool created container " << key 
		 << " with size=" << container->size());
  return StatusCode::SUCCESS; 
}
