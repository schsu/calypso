//Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef WAVEBYTESTREAM_RAWWAVEFORMDECODERTOOL_H
#define WAVEBYTESTREAM_RAWWAVEFORMDECODERTOOL_H

#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "EventFormats/DAQFormats.hpp"
#include "WaveRawEvent/RawWaveformContainer.h"

#include "Identifier/Identifier.h"
#include "FaserCaloIdentifier/EcalID.h"
#include "ScintIdentifier/VetoID.h"
#include "ScintIdentifier/TriggerID.h"
#include "ScintIdentifier/PreshowerID.h"

// This class provides conversion between bytestream and Waveform objects

class RawWaveformDecoderTool : public AthAlgTool {

 public:
  RawWaveformDecoderTool(const std::string& type, const std::string& name, 
			  const IInterface* parent);

  virtual ~RawWaveformDecoderTool();

  static const InterfaceID& interfaceID();

  virtual StatusCode initialize();
  virtual StatusCode finalize();

  StatusCode convert(const DAQFormats::EventFull* re, RawWaveformContainer* wfm, std::string key);

private:
  // List of channels to include in each container
  // List order must correspond to offline channel order
  // All L/R designations refer to looking at the detector from
  // the beam direction.
  //
  // In general, the ordering is layer (longitudinal), row (vertical), module (horizontal)
  // Layers increase with longitudianl position downstream
  // Rows increase from bottom to top
  // Modules increase from right to left
  //
  // For all lists, use invalid channel (-1) to indicate detectors
  // missing in sequence (i.e. 3 of 4 veto counters)
  // 
  // TI12 detector:
  // Calorimeter order:
  //   bottom right, bottom left, top right, top left
  // Veto 
  //   front to back.  
  // Trigger
  //   bottom right PMT, bottom left PMT, top right PMT, top left PMT
  // Preshower
  //   front to back
  //   
  // 2021 Testbeam detector:
  // Calo order:
  //   bottom right, bottom center, bottom left, top R, top C, top L
  // All others are just in order front to back 

  std::vector<int> m_caloChannels;
  std::vector<int> m_vetoChannels;
  std::vector<int> m_triggerChannels;
  std::vector<int> m_preshowerChannels;
  std::vector<int> m_testChannels;
  std::vector<int> m_clockChannels;

  // Identifiers keyed by digitizer channel
  std::map<unsigned int, Identifier> m_identifierMap;

  // ID helpers
  const EcalID* m_ecalID{nullptr};
  const VetoID* m_vetoID{nullptr};
  const TriggerID* m_triggerID{nullptr};
  const PreshowerID* m_preshowerID{nullptr};

};

#endif  /* WAVEBYTESTREAM_FASERTRIGGERDECODERTOOL_H */
 
