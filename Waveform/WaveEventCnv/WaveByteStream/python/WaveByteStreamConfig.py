# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def WaveByteStreamCfg(configFlags, **kwargs):

    acc = ComponentAccumulator()

    # Decide which waveform mapping to instantiate from Geo tag
    
    if configFlags.Input.isMC:
        # Nothing to do for MC
        print("WaveByteStreamConfig.WaveByteStreamCfg - Called for isMC True, nothing to do...")
        return acc
        
    print("WaveByteStreamConfig.WaveByteStreamCfg - Found FaserVersion: ", configFlags.GeoModel.FaserVersion,)

    # Channels are ordered front->back, bottom->top, right->left
    if configFlags.GeoModel.FaserVersion == "FASER-TB00":
        print(" - setting up testbeam detector")

        waveform_tool = CompFactory.RawWaveformDecoderTool("RawWaveformDecoderTool")
        waveform_tool.CaloChannels = [5, 3, 1, 4, 2, 0]
        waveform_tool.VetoChannels = []
        waveform_tool.TriggerChannels = [8, 9]
        waveform_tool.PreshowerChannels = [6, 7]
        acc.addPublicTool(waveform_tool)
        
    elif configFlags.GeoModel.FaserVersion == "FASER-01":
        print(" - setting up TI12 detector")

        waveform_tool = CompFactory.RawWaveformDecoderTool("RawWaveformDecoderTool")
        waveform_tool.CaloChannels = [1, 0, 3, 2]
        waveform_tool.VetoChannels = [4, 5, 6, 7]
        waveform_tool.TriggerChannels = [9, 8, 11, 10]
        waveform_tool.PreshowerChannels = [12, 13]
        acc.addPublicTool(waveform_tool)

    else:
        print(" - unknown version: user must set up Waveform channel mapping by hand!")

    return acc
