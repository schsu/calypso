template<class CONT>
StatusCode IPseudoSimToWaveformRecTool::reconstruct(const CONT* hitCollection,
						    xAOD::WaveformHitContainer* container) const {


  // Check the container
  if (!container) {
    MsgStream log(&(*m_msgSvc), name());
    log << MSG::ERROR << "HitCollection passed to reconstruct() is null!" << endmsg;
    return StatusCode::FAILURE;
  }

  std::map<int, float> idSums;

  // Sum hits in each "channel" 
  for (const auto& hit : *hitCollection) {
    idSums[hit.identify()] += hit.energyLoss();   
  }

  for (const auto& id : idSums) {
      xAOD::WaveformHit* hit = new xAOD::WaveformHit();
      container->push_back(hit);

      hit->set_id(id.first);
      hit->set_channel(0);
      hit->set_peak(0);
      hit->set_mean(0);
      hit->set_width(0);
      hit->set_integral(id.second); 
      hit->set_localtime(0);
      hit->set_raw_peak(0);
      hit->set_raw_integral(0); 
  }

  return StatusCode::SUCCESS;
}
