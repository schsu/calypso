/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file WaveformReconstructionTool.cxx
 * Implementation file for the WaveformReconstructionTool.cxx
 * @ author E. Torrence, 2021
 **/

#include "WaveformReconstructionTool.h"

#include "TH1F.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TGraph.h"
#include "TError.h"

#include <vector>
#include <tuple>
#include <math.h>

// Constructor
WaveformReconstructionTool::WaveformReconstructionTool(const std::string& type, const std::string& name, const IInterface* parent) :
  base_class(type, name, parent)
{
}

// Initialization
StatusCode
WaveformReconstructionTool::initialize() {
  ATH_MSG_INFO( name() << "::initalize()" );

  gErrorIgnoreLevel = kFatal;  // STFU!

  if (m_useSimpleBaseline.value()) {
    ATH_MSG_INFO("Will use simple baseline estimation");
  } else {
    ATH_MSG_INFO("Will use fit to determine baseline");
  }
  return StatusCode::SUCCESS;
}

// Reconstruction step
StatusCode
WaveformReconstructionTool::reconstructAll(
	 const RawWaveformContainer& waveContainer, 
	 const xAOD::WaveformClock* clock, 
	 xAOD::WaveformHitContainer* hitContainer) const {

  ATH_MSG_DEBUG(" reconstructAll called ");

  // Reconstruct each waveform
  for( const auto& wave : waveContainer) {

    ATH_MSG_DEBUG("Reconstruct waveform for channel " << wave->channel());

    // Reconstruct the hits, may be more than one, so pass container
    CHECK( this->reconstruct(*wave, clock, hitContainer) );
  }

  if (m_ensureChannelHits) {
    ATH_MSG_DEBUG("Ensure all channels have hits at peak time");
    ensureHits(waveContainer, clock, hitContainer);
  }

  return StatusCode::SUCCESS;
}

//
// Make sure we have a hit for each channel at the time when
// there is a significant pulse found in the detector
//
void
WaveformReconstructionTool::ensureHits(
	 const RawWaveformContainer& waveContainer, 
	 const xAOD::WaveformClock* clock, 
	 xAOD::WaveformHitContainer* hitContainer) const {

  ATH_MSG_DEBUG(" ensureHits called ");

  // Find peak time (most significant hit)
  xAOD::WaveformHit* peakHit = NULL;

  for( const auto& hit : *hitContainer) {

    if (peakHit == NULL) {
      peakHit = hit;
    } else {
      if ( hit->peak() > peakHit->peak() ) peakHit = hit;
    }

  }

  // Didn't find anything?
  if (peakHit == NULL) return;
  if (peakHit->status_bit(xAOD::WaveformStatus::THRESHOLD_FAILED)) return;

  ATH_MSG_DEBUG("Found peak hit in channel " << peakHit->channel() << " at time " << peakHit->localtime());

  // Now go through all of the channels and check if there is a hit
  // close in time to the peakHit
  for( const auto& wave : waveContainer) {

    // Don't worry about the peak channel, we know this has a hit...
    if (wave->channel() == peakHit->channel()) continue;

    ATH_MSG_DEBUG("Checking for hit in channel " << wave->channel());

    bool found = false;
    // Look for a baseline-only hit that we can update
    xAOD::WaveformHit* baselineHit = NULL;

    // There aren't so many hits, just loop over container
    for( const auto& hit : *hitContainer) {
      if (hit->channel() != wave->channel()) continue;

      // Is this above threshold?
      if (hit->status_bit(xAOD::WaveformStatus::THRESHOLD_FAILED)) {
	baselineHit = hit;
	continue;
      }

      // OK, this is the right channel, check the time
      float dtime = abs(hit->localtime() - peakHit->localtime());
      if (dtime > m_hitTimeDifference) continue;

      // We have found a hit in the right channel at the right time
      found = true;
      ATH_MSG_DEBUG("Found hit in channel " << hit->channel() 
		    << " at time " << hit->localtime());
      break;
    }

    // Is there a hit?  If so, go to next waveform/channel
    if (found) continue;

    ATH_MSG_DEBUG("No hit found for channel " << wave->channel() 
		  << " at time " << peakHit->localtime());

    // Do we have a baseline-only hit we can use?
    xAOD::WaveformHit* newhit = NULL;
    if (baselineHit == NULL) {
      // No, make a new hit here
      newhit = new xAOD::WaveformHit();
      hitContainer->push_back(newhit);

      // Mark this as a secondary hit
      newhit->set_status_bit(xAOD::WaveformStatus::THRESHOLD_FAILED);
      newhit->set_status_bit(xAOD::WaveformStatus::SECONDARY);

      // Set digitizer channel and identifier
      newhit->set_channel(wave->channel());
      newhit->set_id(wave->identify32().get_compact());

      // Make sure we have ADC counts
      if (wave->adc_counts().size() == 0) {
	ATH_MSG_WARNING( "Found waveform for channel " << wave->channel() 
			 << " with size " << wave->adc_counts().size() << "!");
	
	newhit->set_status_bit(xAOD::WaveformStatus::WAVEFORM_MISSING);
	continue;
      } 
      
      if (wave->adc_counts().size() != wave->n_samples()) {
	ATH_MSG_WARNING( "Found waveform for channel " << wave->channel() 
			 << " with size " << wave->adc_counts().size() 
			 << " not equal to number of samples " << wave->n_samples());
	
	newhit->set_status_bit(xAOD::WaveformStatus::WAVEFORM_INVALID);
	continue;
      }

      findBaseline(*wave, newhit);

    } else {
      // Use the existing baseline hit
      newhit = baselineHit;
    }

    // Check for problems
    if (newhit->status_bit(xAOD::WaveformStatus::BASELINE_FAILED)) continue;

    // Set range for windowed data
    unsigned int lo_edge = peakHit->time_vector().front()/2.;
    unsigned int hi_edge = peakHit->time_vector().back()/2.;

    ATH_MSG_DEBUG("Windowing waveform from " << lo_edge << " to " << hi_edge);
    std::vector<float> wtime(hi_edge-lo_edge+1);
    std::vector<float> wwave(hi_edge-lo_edge+1);
    for (unsigned int i=lo_edge; i<=hi_edge; i++) {
      unsigned int j = i-lo_edge;
      wtime[j] = 2.*i;
      wwave[j] = newhit->baseline_mean() - wave->adc_counts()[i];
      //ATH_MSG_DEBUG(" Time: " << wtime[j] << " Wave: " << wwave[j]);
    }

    newhit->set_time_vector(wtime);
    newhit->set_wave_vector(wwave);

    //
    // Find some raw values
    WaveformFitResult raw = findRawHitValues(wtime, wwave);
    newhit->set_peak(raw.peak);
    newhit->set_mean(raw.mean);
    newhit->set_width(raw.sigma);
    newhit->set_integral(raw.integral);
    newhit->set_localtime(raw.mean);
    newhit->set_raw_peak(raw.peak);
    newhit->set_raw_integral(raw.integral);

    //
    // Find time from clock
    if (!clock || (clock->frequency() <= 0.)) {
      newhit->set_status_bit(xAOD::WaveformStatus::CLOCK_INVALID);
      newhit->set_bcid_time(-1.);
    } else {
      newhit->set_bcid_time(clock->time_from_clock(newhit->localtime()));
    }

  } // End of loop over waveContainer
}

// Find the baseline
WaveformBaselineData& 
WaveformReconstructionTool::findBaseline(const RawWaveform& raw_wave, 
					 xAOD::WaveformHit* hit) const {

  //
  // Find baseline
  static WaveformBaselineData baseline;

  if (m_useSimpleBaseline.value())
    baseline = findSimpleBaseline(raw_wave);
  else
    baseline = findAdvancedBaseline(raw_wave);

  if (!(baseline.valid)) {
    ATH_MSG_WARNING("Failed to reconstruct baseline!");

    hit->set_baseline_mean(0.);
    hit->set_baseline_rms(-1.);
    hit->set_status_bit(xAOD::WaveformStatus::BASELINE_FAILED);

  } else {
    // Save baseline to hit collection object
    hit->set_baseline_mean(baseline.mean);
    hit->set_baseline_rms(baseline.rms);
  }

  return baseline;
}

StatusCode
WaveformReconstructionTool::reconstruct(const RawWaveform& raw_wave,
					const xAOD::WaveformClock* clock, 
					xAOD::WaveformHitContainer* container) const {

  ATH_MSG_DEBUG(" reconstruct called ");

  // Check the container
  if (!container) {
    ATH_MSG_ERROR("WaveformHitCollection passed to reconstruct() is null!");
    return StatusCode::FAILURE;
  }

  //
  // We always want to create at least one hit, so create it here
  xAOD::WaveformHit* hit = new xAOD::WaveformHit();
  container->push_back(hit);

  // Set digitizer channel and identifier
  hit->set_channel(raw_wave.channel());
  hit->set_id(raw_wave.identify32().get_compact());

  // Make sure we have ADC counts
  if (raw_wave.adc_counts().size() == 0) {
    ATH_MSG_WARNING( "Found waveform for channel " << raw_wave.channel() 
		     << " with size " << raw_wave.adc_counts().size() << "!");

    hit->set_status_bit(xAOD::WaveformStatus::WAVEFORM_MISSING);
    return StatusCode::SUCCESS;
  } 

  if (raw_wave.adc_counts().size() != raw_wave.n_samples()) {
    ATH_MSG_WARNING( "Found waveform for channel " << raw_wave.channel() 
		     << " with size " << raw_wave.adc_counts().size() 
		     << " not equal to number of samples " << raw_wave.n_samples());

    hit->set_status_bit(xAOD::WaveformStatus::WAVEFORM_INVALID);
    return StatusCode::SUCCESS;
  }

  // Find the baseline
  WaveformBaselineData baseline = findBaseline(raw_wave, hit);

  // Check that we have data to work with
  // If any status bits are set, this is bad
  if (hit->status()) return StatusCode::SUCCESS;

  //
  // Create baseline-subtracted data array for both time and signal
  // Time in ns from start of readout
  unsigned int size = raw_wave.adc_counts().size();
  std::vector<float> time(size);  
  for (unsigned int i=0; i<size; i++)
    time[i] = 2.*i;

  // Baseline subtracted (and inverted) ADC waveform values
  std::vector<float> wave(raw_wave.adc_counts().begin(), raw_wave.adc_counts().end());
  for (auto& element : wave)
    element = baseline.mean - element;

  bool first = true;

  // Now we iteratively find peaks and fit
  while(true) {

    //
    // Find peak in array and return time and value arrays
    // This range of data is also *removed* from original arrays
    std::vector<float> wtime;
    std::vector<float> wwave;

    // All done if we don't have any peaks above threshold
    // If we do find a significant peak, fill the window
    if (! findPeak(baseline, time, wave, wtime, wwave) ) {
      if (first) hit->set_status_bit(xAOD::WaveformStatus::THRESHOLD_FAILED);
      break;
    }

    //
    // Create new hit to fill
    if (!first) {
      hit = new xAOD::WaveformHit();
      container->push_back(hit);
      hit->set_status_bit(xAOD::WaveformStatus::SECONDARY);
    }
    first = false;

    //
    // Save windowed waveform to Hit object
    hit->set_channel(raw_wave.channel());
    hit->set_baseline_mean(baseline.mean);
    hit->set_baseline_rms(baseline.rms);
    hit->set_time_vector(wtime);
    hit->set_wave_vector(wwave);

    //
    // Find some raw values
    WaveformFitResult raw = findRawHitValues(wtime, wwave);
    hit->set_peak(raw.peak);
    hit->set_mean(raw.mean);
    hit->set_width(raw.sigma);
    hit->set_integral(raw.integral);
    hit->set_localtime(raw.mean);
    hit->set_raw_peak(raw.peak);
    hit->set_raw_integral(raw.integral);

    //
    // Perform Gaussian fit to waveform
    WaveformFitResult gfit = fitGaussian(raw, wtime, wwave);
    if (! gfit.valid) {
      // Lets try again with a more restricted width
      ATH_MSG_WARNING( " Gaussian waveform fit failed with width " << raw.sigma << " try reducing width to 1 " );
      raw.sigma = 1.;
      gfit = fitGaussian(raw, wtime, wwave);
      if (!gfit.valid) {
	hit->set_status_bit(xAOD::WaveformStatus::GFIT_FAILED);
      }
    } 

    // Fit results (or raw if it failed)
    hit->set_peak(gfit.peak);
    hit->set_mean(gfit.mean);
    hit->set_width(gfit.sigma);
    hit->set_integral(gfit.integral);
    hit->set_localtime(gfit.time);

    //
    // Check for overflow
    if (m_removeOverflow && findOverflow(baseline, wtime, wwave)) {
      ATH_MSG_INFO("Found waveform overflow");
      hit->set_status_bit(xAOD::WaveformStatus::WAVE_OVERFLOW);
    }

    //
    // Perform CB fit
    WaveformFitResult cbfit = fitCBall(gfit, wtime, wwave);
    if (! cbfit.valid) {
      ATH_MSG_WARNING("CrystalBall fit failed!");
      // Still have gaussian parameters as an estimate
      hit->set_status_bit(xAOD::WaveformStatus::CBFIT_FAILED);
    } else {
      hit->set_peak(cbfit.peak);
      hit->set_mean(cbfit.mean);
      hit->set_width(cbfit.sigma);
      hit->set_integral(cbfit.integral);
      hit->set_localtime(cbfit.time);

      hit->set_alpha(cbfit.alpha);
      hit->set_nval(cbfit.nval);
    }

    //
    // Find time from clock
    if (!clock || (clock->frequency() <= 0.)) {
      hit->set_status_bit(xAOD::WaveformStatus::CLOCK_INVALID);
      hit->set_bcid_time(-1.);
    } else {
      hit->set_bcid_time(clock->time_from_clock(hit->localtime()));
    }

    if (! m_findMultipleHits) break;

  } // End of loop over waveform data

  ATH_MSG_DEBUG( "WaveformReconstructionTool finished for channel " 
		 << raw_wave.channel() << " container size= " << container->size());

  return StatusCode::SUCCESS;
}

bool
WaveformReconstructionTool::findPeak(WaveformBaselineData& baseline, 
   std::vector<float>& time, std::vector<float>& wave,
   std::vector<float>& windowed_time, std::vector<float>& windowed_wave) const {

  ATH_MSG_DEBUG("findPeak called");

  // Find max value location in array
  unsigned int imax = std::max_element(wave.begin(), wave.end()) - wave.begin();
  float maxval = wave[imax];
  ATH_MSG_DEBUG( "Found peak value " << maxval << " at position " << imax );

  // Check if this is over threshold (in sigma)
  if (maxval < m_peakThreshold*baseline.rms) {
    ATH_MSG_DEBUG("Failed threshold");
    return false;
  }

  // Make a window around this peak, values are in bins, so units of 2ns
  // Ensure our window is within the vector range
  int lo_edge = ((int(imax) + m_windowStart) >= 0 ? (imax + m_windowStart) : 0); 
  int hi_edge = ((imax + m_windowStart + m_windowWidth) < wave.size() ? (imax + m_windowStart + m_windowWidth) : wave.size());
  
  ATH_MSG_DEBUG("Windowing waveform from " << lo_edge << " to " << hi_edge);
  windowed_time = std::vector<float> (time.begin()+lo_edge, time.begin()+hi_edge);
  windowed_wave = std::vector<float> (wave.begin()+lo_edge, wave.begin()+hi_edge);

  // Remove these values from the original arrays so we can iterate
  time.erase(time.begin()+lo_edge, time.begin()+hi_edge);
  wave.erase(wave.begin()+lo_edge, wave.begin()+hi_edge);

  return true;
}

bool
WaveformReconstructionTool::findOverflow(const WaveformBaselineData& base, 
	      std::vector<float>& time, std::vector<float>& wave) const {

  auto peakloc = std::max_element(wave.begin(), wave.end());

  // If peak value is less than baseline, we have no overflow
  if (*peakloc < int(base.mean)) return false;

  ATH_MSG_DEBUG("Removing overflows from waveform with length " << wave.size());

  // We have an overflow, remove all elements that are overflowing
  unsigned int i = peakloc - wave.begin();
  for (; i<wave.size(); i++) {
    if (wave[i] < int(base.mean)) continue;

    ATH_MSG_DEBUG("Removing position "<< i<< " with value " << wave[i] << " > " << int(base.mean));
    // This is an overflow, remove elements
    time.erase(time.begin() + i);
    wave.erase(wave.begin() + i);
    i--;  // Decrement so that loop itertaion points to next element
  }

  ATH_MSG_DEBUG("Removed overflows, length now " << wave.size());
  return true;
}

WaveformBaselineData&
WaveformReconstructionTool::findSimpleBaseline(const RawWaveform& raw_wave) const {

  ATH_MSG_DEBUG( "findSimpleBaseline called" );
  //ATH_MSG_DEBUG( raw_wave );

  // This must be static so we can return a reference
  static WaveformBaselineData baseline;  
  baseline.clear();

  // Calling algorithm checks for proper data
  // Here we just check algorithm-specific issues
  if (int(raw_wave.n_samples()) < m_samplesForBaselineAverage.value()) {
    ATH_MSG_WARNING( "Found waveform with " << raw_wave.n_samples() << " samples, not enough to find baseline!" );
    return baseline;
  }

  const std::vector<unsigned int>& counts = raw_wave.adc_counts();

  double sumx = 0.;
  double sumx2 = 0.;

  // Look at first n values in the waveform
  unsigned int nvalues = m_samplesForBaselineAverage.value();

  for (unsigned int i=0; i< nvalues; i++) {
    sumx  += counts[i];
    sumx2 += (counts[i] * counts[i]);
  }
  double mean = sumx / nvalues;
  double mean2 = sumx2 / nvalues;

  double rms = std::sqrt(mean2 - mean*mean);

  baseline.valid = true;
  baseline.channel = raw_wave.channel();
  baseline.mean = mean;
  baseline.rms = rms;
  return baseline;
}

WaveformBaselineData&
WaveformReconstructionTool::findAdvancedBaseline(const RawWaveform& raw_wave) const {

  ATH_MSG_DEBUG( "findAdvancedBaseline called" );

  // This must be static so we can return a reference
  static WaveformBaselineData baseline;  
  baseline.clear();

  // First histogram to find most likely value
  // Turn these into configurables once this works
  int nbins = m_baselineRangeBins;
  double xlo = 0.;
  double xhi = m_baselineRange;

  TH1F h1("", "", nbins, xlo, xhi);

  // Fill this histogram with the waveform
  // Only use a subset, based on where we think there won't be signal
  const std::vector<unsigned int>& counts = raw_wave.adc_counts();
  for (int i=m_baselineSampleLo; i<=m_baselineSampleHi; i++) {
    //ATH_MSG_INFO( "Entry " << i << " Value " << counts[i] );
    h1.Fill(counts[i]);
  }

  /*
  for (auto value : raw_wave.adc_counts()) {
    //ATH_MSG_DEBUG( "Found value " << value );
    h1.Fill(value);
  }
  */

  // Find max bin
  int maxbin = h1.GetMaximumBin();
  double maxbinval = h1.GetXaxis()->GetBinCenter(maxbin);
  ATH_MSG_DEBUG( "Found max bin at " << maxbinval << " counts");

  // Fill second histogram with integer resolution around the peak
  nbins = m_baselineFitRange;
  xlo = int(maxbinval - nbins/2)-0.5;
  xhi = xlo + nbins;
  ATH_MSG_DEBUG( "Filling 2nd histogram from " << xlo << " to " << xhi);

  TH1F h2("", "", nbins, xlo, xhi);
  for (int i=m_baselineSampleLo; i<=m_baselineSampleHi; i++) {
    h2.Fill(counts[i]);
  }

  // Start with full histogram range
  double mean = h2.GetMean();
  double rms = h2.GetRMS();
  double peak = h2.GetBinContent(h2.GetMaximumBin());

  ATH_MSG_DEBUG( "Initial Mean: " << mean << " RMS: " << rms << " Peak: " << peak );

  // Restrict range to +/- 2 sigma of mean
  double window = m_baselineFitWindow;  // Window range in sigma
  h2.GetXaxis()->SetRangeUser(mean-window*rms, mean+window*rms);
  mean = h2.GetMean();
  rms = h2.GetRMS();
  peak = h2.GetBinContent(h2.GetMaximumBin());

  ATH_MSG_DEBUG( "2 Sigma Mean: " << mean << " RMS: " << rms << " Peak: " << peak);

  // Finally, fit a Gaussian to the 2 sigma range
  double fitlo = mean-window*rms;
  double fithi = mean+window*rms;

  // Define fit function and preset range
  TF1 fitfunc("BaseFit", "gaus", fitlo, fithi);
  fitfunc.SetParameters(peak, mean, rms);

  // Fit Options:
  // L - Likelihood fit
  // Q - Quiet mode (V for verbose)
  // N - Don't draw or store graphics
  // S - Return fit results
  // E - Better error estimation
  // M - Use TMinuit IMPROVE to find a better minimum
  // R - Use function range  
  TFitResultPtr fit = h2.Fit(&fitfunc, "LQNSR", "");

  int fitStatus = fit;
  double chi2 = fit->Chi2();
  unsigned int ndf = fit->Ndf();
  if (ndf == 0) ndf = 1;

  if (!fit->IsValid()) {
    ATH_MSG_WARNING( " Baseline fit failed! ");
  } else {
    // Improve estimation with fit results
    peak = fit->Parameter(0);
    mean = fit->Parameter(1);
    rms  = fit->Parameter(2);
    ATH_MSG_DEBUG( "G Fit   Mean: " << mean << " RMS: " << rms << " Peak: " << peak << " Chi2/N: " << chi2/ndf);  
  }

  baseline.valid = true;
  baseline.channel = raw_wave.channel();
  baseline.mean = mean;
  baseline.rms = rms;
  baseline.fit_status = fitStatus; 
  baseline.peak = peak;
  baseline.chi2n = chi2/ndf;
  return baseline;
}

WaveformFitResult&
WaveformReconstructionTool::findRawHitValues(const std::vector<float> time, const std::vector<float> wave) const {

  ATH_MSG_DEBUG("findRawHitValues called");

  // This must be static so we can return a reference
  static WaveformFitResult rfit;
  rfit.clear();

  // First, estimate starting values from input waveform
  // Will use this to pre-set fit values, but also as a backup in case fit fails
  double tot = 0.;
  double sum = 0.;
  double sum2 = 0.;
  for (unsigned int i=0; i<time.size(); i++) {
    tot += wave[i];
    sum += time[i] * wave[i];
    sum2 += time[i] * time[i] * wave[i];
  }

  // Pointer to peak
  auto peakloc = std::max_element(wave.begin(), wave.end());

  // Initial parameters from waveform
  rfit.mean = sum/tot;
  rfit.sigma = std::sqrt(sum2/tot - rfit.mean*rfit.mean);
  rfit.peak = *peakloc;
  rfit.integral = 2*tot; // Factor of 2 because at 500 MHz, dt = 2 ns
  rfit.time = rfit.mean;

  ATH_MSG_DEBUG( 
		"Initial Mean: " << rfit.mean 
		<< " RMS: " << rfit.sigma 
		<< " Peak: " << rfit.peak 
		<< " Integral: " << rfit.integral
		 );  

  // Fix bad values
  if (isnan(rfit.sigma)) {
    ATH_MSG_DEBUG("Found sigma: " << rfit.sigma << " Replace with 2.");
    rfit.sigma = 2.;
  }

  if (rfit.mean < time.front() || rfit.mean > time.back()) {
    rfit.mean = (3*time.front() + time.back())/4.; // Set time to 1/4 of way through window
    ATH_MSG_DEBUG("Found mean: " << rfit.time << " out of range " << time.front() << "-" << time.back() << " Replace with " << rfit.mean);
    rfit.time = rfit.mean;
  }

  return rfit;
}

WaveformFitResult&
WaveformReconstructionTool::fitGaussian(const WaveformFitResult& raw, const std::vector<float> time, const std::vector<float> wave) const {

  ATH_MSG_DEBUG("fitGaussian called");

  // This must be static so we can return a reference
  static WaveformFitResult gfit;
  gfit.clear();

  // Start with raw values by default
  gfit = raw;

  // Start by creating a TGraph and fitting this with a Gaussian
  // Must pass arrays to TGraph, use pointer to first vector element
  TGraph tg(time.size(), &time[0], &wave[0]);

  // Define fit function and preset range
  TF1 gfunc("gfunc", "gaus");
  gfunc.SetParameters(raw.peak, raw.mean, raw.sigma);
  gfunc.SetParError(0, std::sqrt(raw.peak));
  gfunc.SetParError(1, raw.sigma);
  gfunc.SetParError(2, raw.sigma / 5.);
  gfunc.SetParLimits(2, 0., 20.);  // Constrain width

  TFitResultPtr gfitptr = tg.Fit(&gfunc, "QNS", "");

  gfit.fit_status = gfitptr;
  gfit.valid = (gfit.fit_status == 0);

  if (!gfitptr->IsValid()) {
    ATH_MSG_WARNING( " Gaussian waveform fit failed! ");
  } else {
    // Improve estimation with fit results
    gfit.peak = gfitptr->Parameter(0);
    gfit.mean = gfitptr->Parameter(1);
    gfit.sigma = gfitptr->Parameter(2);
    gfit.integral = gfunc.Integral(time.front(), time.back());

    // Find time here
    gfit.time = gfunc.GetX( gfit.peak * m_timingPeakFraction, time.front(), gfit.mean );

    ATH_MSG_DEBUG("G Fit   Mean: " << gfit.mean << " RMS: " << gfit.sigma 
		  << " Peak: " << gfit.peak << " Int: " << gfit.integral << " Time: " << gfit.time);
  }

  return gfit;
}

WaveformFitResult&
WaveformReconstructionTool::fitCBall(const WaveformFitResult& gfit, 
     const std::vector<float> time, const std::vector<float> wave) const {

  ATH_MSG_DEBUG("fitCBall called");

  // This must be static so we can return a reference
  static WaveformFitResult cbfit;
  cbfit.clear();

  // Must pass arrays to TGraph, use pointer to first vector element
  TGraph tg(time.size(), &time[0], &wave[0]);

  // Values to preset CB fit
  cbfit.peak = abs(gfit.peak);
  cbfit.mean = gfit.mean;
  cbfit.sigma = abs(gfit.sigma);
  if (cbfit.sigma > 20.) cbfit.sigma = 2.;
  cbfit.alpha = -0.5;  // Negative to get tail on high side
  cbfit.nval = 2.;

  // Define fit function and preset values
  TF1 cbfunc("cbfunc", "crystalball");
  cbfunc.SetParameter(0, cbfit.peak); // Peak height
  cbfunc.SetParError(0, std::sqrt(cbfit.peak));
  cbfunc.SetParameter(1, cbfit.mean); // Mean
  cbfunc.SetParError(1, cbfit.sigma);
  cbfunc.SetParameter(2, cbfit.sigma);  // Width
  cbfunc.SetParError(2, cbfit.sigma/5.);
  cbfunc.SetParLimits(2, 0., 20.);
  cbfunc.SetParameter(3, cbfit.alpha); // Tail parameter (negative for high-side tail)
  cbfunc.SetParError(3, -cbfit.alpha/5.);
  cbfunc.SetParLimits(3, -10., 0.);
  // Try fixing the tail first
  cbfunc.FixParameter(4, cbfit.nval);

  TFitResultPtr cbfitptr = tg.Fit(&cbfunc, "QNS", "");

  if (!cbfitptr->IsValid()) {
    ATH_MSG_WARNING( " First Crystal Ball waveform fit failed! ");
  }

  // Now try releasing the tail parameter
  cbfunc.ReleaseParameter(4);
  cbfunc.SetParameter(4, cbfit.nval);  // Tail power
  cbfunc.SetParError(4, cbfit.nval/5.);
  cbfunc.SetParLimits(4, 0., 1.E3);

  cbfitptr = tg.Fit(&cbfunc, "QNS", "");

  cbfit.fit_status = cbfitptr;
  cbfit.valid = (cbfit.fit_status == 0);

  if (!cbfitptr->IsValid()) {
    ATH_MSG_WARNING( " Crystal Ball waveform fit failed! ");
  } else {
    // Improve estimation with fit results
    cbfit.peak = cbfitptr->Parameter(0);
    cbfit.mean = cbfitptr->Parameter(1);
    cbfit.sigma = cbfitptr->Parameter(2);
    cbfit.alpha = cbfitptr->Parameter(3);
    cbfit.nval = cbfitptr->Parameter(4);

    double chi2 = cbfitptr->Chi2();
    unsigned int ndf = cbfitptr->Ndf();
    if (ndf == 0) ndf = 1;
    cbfit.chi2ndf = chi2/ndf;

    cbfit.integral = cbfunc.Integral(time.front(), time.back());

    // Find time here
    cbfit.time = cbfunc.GetX( cbfit.peak * m_timingPeakFraction, time.front(), cbfit.mean );

    ATH_MSG_DEBUG("CB Fit  Mean: " << cbfit.mean << " RMS: " << cbfit.sigma 
		  << " Peak: " << cbfit.peak << " Int: " << cbfit.integral
		  << " Time: " << cbfit.time 
		  << " N: " << cbfit.nval << " Alpha: " << cbfit.alpha 
		  << " Chi2/Ndf: " << cbfit.chi2ndf );

  }

  return cbfit;
}
