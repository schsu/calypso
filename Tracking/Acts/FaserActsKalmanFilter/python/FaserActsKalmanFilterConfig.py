# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS and FASER collaborations

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from MagFieldServices.MagFieldServicesConfig import MagneticFieldSvcCfg

FaserActsKalmanFilterAlg,FaserActsTrackingGeometrySvc, FaserActsTrackingGeometryTool, FaserActsExtrapolationTool,FaserActsAlignmentCondAlg, THistSvc = CompFactory.getComps("FaserActsKalmanFilterAlg","FaserActsTrackingGeometrySvc", "FaserActsTrackingGeometryTool", "FaserActsExtrapolationTool","FaserActsAlignmentCondAlg", "THistSvc")

def FaserActsTrackingGeometrySvcCfg(flags, **kwargs):
    acc = ComponentAccumulator()
    acc.addService(FaserActsTrackingGeometrySvc(name = "FaserActsTrackingGeometrySvc", **kwargs))
    return acc 

def FaserActsAlignmentCondAlgCfg(flags, **kwargs):
    acc = ComponentAccumulator()
    acc.addCondAlgo(CompFactory.FaserActsAlignmentCondAlg(name = "FaserActsAlignmentCondAlg", **kwargs))
    #acc.addCondAlgo(CompFactory.NominalAlignmentCondAlg(name = "NominalAlignmentCondAlg", **kwargs))
    return acc

def FaserActsKalmanFilterCfg(flags, **kwargs):
    acc = ComponentAccumulator()
    # Initialize field service
    acc.merge(MagneticFieldSvcCfg(flags))

    acc.merge(FaserActsTrackingGeometrySvcCfg(flags, **kwargs))
    acc.merge(FaserActsAlignmentCondAlgCfg(flags, **kwargs))
    
    kwargs.setdefault("FaserSpacePointsSeedsName", "Seeds_SpacePointContainer")
    actsKalmanFilterAlg = FaserActsKalmanFilterAlg(**kwargs)
    #actsKalmanFilterAlg.TrackingGeometryTool = FaserActsTrackingGeometryTool("TrackingGeometryTool")
    actsExtrapolationTool=FaserActsExtrapolationTool("FaserActsExtrapolationTool")
    actsExtrapolationTool.FieldMode="FASER"
    #actsExtrapolationTool.FieldMode="Constant"
    actsExtrapolationTool.TrackingGeometryTool=FaserActsTrackingGeometryTool("TrackingGeometryTool")
    actsKalmanFilterAlg.ExtrapolationTool=actsExtrapolationTool
    acc.addEventAlgo(actsKalmanFilterAlg)
    histSvc= THistSvc()
    histSvc.Output += [ "KalmanTracks DATAFILE='KalmanTracks.root' OPT='RECREATE'" ]
    acc.addService(histSvc)
    acc.merge(FaserActsKalmanFilter_OutputCfg(flags))
    return  acc

def FaserActsKalmanFilter_OutputCfg(flags):
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    acc.merge(OutputStreamCfg(flags, "ESD"))
    ostream = acc.getEventAlgo("OutputStreamESD")
    ostream.TakeItemsFromInput = True
    return acc
