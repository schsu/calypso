# Declare the package name:
atlas_subdir(FaserActsKalmanFilter)

# External dependencies:
find_package( CLHEP )
find_package( Eigen )
find_package( Boost )
find_package( Acts COMPONENTS Core )

# Component(s) in the package:
atlas_add_library( FaserActsKalmanFilterLib
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    AthenaKernel
    ActsCore
    TrackerIdentifier
    TrackerReadoutGeometry
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
)

atlas_add_component(FaserActsKalmanFilter
    FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h
    FaserActsKalmanFilter/FaserActsGeometryContainers.h
    FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h
    FaserActsKalmanFilter/IndexSourceLink.h
    FaserActsKalmanFilter/Measurement.h
    FaserActsKalmanFilter/SimWriterTool.h
    FaserActsKalmanFilter/SPSeedBasedInitialParameterTool.h
    FaserActsKalmanFilter/SPSimpleInitialParameterTool.h
    FaserActsKalmanFilter/TrajectoryWriterTool.h
    FaserActsKalmanFilter/TruthBasedInitialParameterTool.h
    src/CombinatorialKalmbanFilterAlg.cxx
    src/SimWriterTool.cxx
    src/SPSeedBasedInitialParameterTool.cxx
    src/SPSimpleInitialParameterTool.cxx
    src/TrackFindingAlgorithmFunction.cxx
    src/TrajectoryWriterTool.cxx
    src/TruthBasedInitialParameterTool.cxx
    src/components/FaserActsKalmanFilter_entries.cxx
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    ActsCore
    EventInfo
    FaserActsKalmanFilterLib
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
    MagFieldInterfaces
    MagFieldElements
    MagFieldConditions
    TrkPrepRawData
    TrkTrack
    TrackerPrepRawData
    TrackerRIO_OnTrack
    TrkRIO_OnTrack
    TrackerSpacePoint
    GeoModelFaserUtilities
    GeneratorObjects
    TrackerIdentifier
    TrackerReadoutGeometry
    Identifier
    TrackerSimData
    TrackerSeedFinderLib
)

# Install files from the package:
atlas_install_headers(FaserActsKalmanFilter)
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
