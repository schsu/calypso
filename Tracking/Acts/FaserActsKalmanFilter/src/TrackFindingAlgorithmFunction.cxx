#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"
#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/MagneticField/InterpolatedBFieldMap.hpp"
#include "Acts/MagneticField/SharedBField.hpp"

#include <boost/variant/variant.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/variant/static_visitor.hpp>


using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;

namespace ActsExtrapolationDetail {
  using VariantPropagatorBase = boost::variant<
      Acts::Propagator<Acts::EigenStepper<>, Acts::Navigator>,
      Acts::Propagator<Acts::EigenStepper<>, Acts::Navigator>
  >;

  class VariantPropagator : public VariantPropagatorBase
  {
  public:
    using VariantPropagatorBase::VariantPropagatorBase;
  };

}

using ActsExtrapolationDetail::VariantPropagator;


template <typename TrackFinder>
struct TrackFinderFunctionImpl {
  TrackFinder trackFinder;

  TrackFinderFunctionImpl(TrackFinder&& f) : trackFinder(std::move(f)) {}

  CombinatorialKalmanFilterAlg::TrackFinderResult operator()(
      const IndexSourceLinkContainer& sourceLinks,
      const std::vector<Acts::CurvilinearTrackParameters>& initialParameters,
      const CombinatorialKalmanFilterAlg::TrackFinderOptions& options) const
  {
    return trackFinder.findTracks(sourceLinks, initialParameters, options);
  }
};


CombinatorialKalmanFilterAlg::TrackFinderFunction 
CombinatorialKalmanFilterAlg::makeTrackFinderFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry)
{
  const std::string fieldMode = "FASER";
  const std::vector<double> constantFieldVector = {0., 0., 0.55};

  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive   = false;
  cfg.resolveMaterial  = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator( cfg );

  std::unique_ptr<ActsExtrapolationDetail::VariantPropagator> varProp;

  if (fieldMode == "FASER") {
    auto bField = std::make_shared<FASERMagneticFieldWrapper>();
    auto stepper = Acts::EigenStepper<>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper), Acts::Navigator>(std::move(stepper),
                                                                      std::move(navigator));
    varProp = std::make_unique<VariantPropagator>(propagator);
  }
  else if (fieldMode == "Constant") {
    Acts::Vector3 constantFieldVector = Acts::Vector3(constantFieldVector[0], 
                                                      constantFieldVector[1], 
                                                      constantFieldVector[2]);

    auto bField = std::make_shared<Acts::ConstantBField>(constantFieldVector);
    auto stepper = Acts::EigenStepper<>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper), Acts::Navigator>(std::move(stepper),
                                                                      std::move(navigator));
    varProp = std::make_unique<VariantPropagator>(propagator);
  }

  return boost::apply_visitor([&](const auto& propagator) -> TrackFinderFunction {
    using Updater  = Acts::GainMatrixUpdater;
    using Smoother = Acts::GainMatrixSmoother;
    using CKF = Acts::CombinatorialKalmanFilter<typename std::decay_t<decltype(propagator)>, Updater, Smoother>;

    CKF trackFinder(std::move(propagator));

    return TrackFinderFunctionImpl<CKF>(std::move(trackFinder));
  }, *varProp);
}