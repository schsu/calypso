#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
# from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from TrackerSeedFinder.TrackerSeedFinderConfig import TrackerSeedFinderCfg
from FaserActsKalmanFilter.CombinatorialKalmanFilterConfig import CombinatorialKalmanFilterCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['my.RDO.pool.root']
ConfigFlags.Output.ESDFileName = "myCKF.ESD.pool.root"
ConfigFlags.Output.AODFileName = "myCKF.AOD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
# ConfigFlags.GeoModel.FaserVersion = "FASER-01"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
# acc.merge(PoolWriteCfg(ConfigFlags))

acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(TrackerSeedFinderCfg(ConfigFlags))
acc.merge(CombinatorialKalmanFilterCfg(ConfigFlags))

# logging.getLogger('forcomps').setLevel(INFO)
# acc.foreach_component("*").OutputLevel = INFO
# acc.foreach_component("*ClassID*").OutputLevel = INFO
# acc.getService("StoreGateSvc").Dump = True
# acc.getService("ConditionStore").Dump = True
# acc.printConfig(withDetails=True)
# ConfigFlags.dump()

sc = acc.run(maxEvents=1000)

sys.exit(not sc.isSuccess())
