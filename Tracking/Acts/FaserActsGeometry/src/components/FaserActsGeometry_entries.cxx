/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserActsGeometry/FaserActsExtrapolationAlg.h"
#include "FaserActsGeometry/FaserActsWriteTrackingGeometry.h"
#include "FaserActsGeometry/FaserActsTrackingGeometrySvc.h"
#include "FaserActsGeometry/FaserActsExtrapolationTool.h"
#include "FaserActsGeometry/FaserActsObjWriterTool.h"
#include "FaserActsGeometry/FaserActsTrackingGeometryTool.h"
#include "FaserActsGeometry/FaserActsPropStepRootWriterSvc.h"
#include "FaserActsGeometry/FaserActsAlignmentCondAlg.h"
#include "FaserActsGeometry/FaserNominalAlignmentCondAlg.h"
//#include "FaserActsGeometry/FaserActsKalmanFilterAlg.h"
//#include "FaserActsGeometry/FaserActsExCellWriterSvc.h"
//#include "FaserActsGeometry/FaserActsMaterialTrackWriterSvc.h"
//#include "FaserActsGeometry/GeomShiftCondAlg.h"
//#include "FaserActsGeometry/FaserActsWriteTrackingGeometryTransforms.h"
#include "FaserActsGeometry/FaserActsVolumeMappingTool.h"
//#include "FaserActsGeometry/FaserActsMaterialJsonWriterTool.h"
#include "FaserActsGeometry/FaserActsMaterialMapping.h"
#include "FaserActsGeometry/FaserActsSurfaceMappingTool.h"

DECLARE_COMPONENT( FaserActsTrackingGeometrySvc )
DECLARE_COMPONENT( FaserActsTrackingGeometryTool )
DECLARE_COMPONENT( FaserActsExtrapolationAlg )
DECLARE_COMPONENT( FaserActsExtrapolationTool )
DECLARE_COMPONENT( FaserActsPropStepRootWriterSvc )
DECLARE_COMPONENT( FaserActsObjWriterTool )
DECLARE_COMPONENT( FaserActsWriteTrackingGeometry )
DECLARE_COMPONENT( FaserActsAlignmentCondAlg )
//DECLARE_COMPONENT( FaserActsKalmanFilterAlg )
//DECLARE_COMPONENT( FaserNominalAlignmentCondAlg )
//DECLARE_COMPONENT( FaserActsExCellWriterSvc )
//DECLARE_COMPONENT( FaserActsMaterialTrackWriterSvc )
//DECLARE_COMPONENT( FaserActsWriteTrackingGeometryTransforms )
//DECLARE_COMPONENT( FaserGeomShiftCondAlg )
DECLARE_COMPONENT( FaserActsVolumeMappingTool )
//DECLARE_COMPONENT( FaserActsMaterialJsonWriterTool )
DECLARE_COMPONENT( FaserActsMaterialMapping )
DECLARE_COMPONENT( FaserActsSurfaceMappingTool )
