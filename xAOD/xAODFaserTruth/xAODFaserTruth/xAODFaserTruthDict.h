#ifndef XAODFASERTRUTH_XAODFASERTRUTHDICT_H
#define XAODFASERTRUTH_XAODFASERTRUTHDICT_H

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthParticleAuxContainer.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"
#include "xAODFaserTruth/FaserTruthVertexAuxContainer.h"
#include "xAODFaserTruth/FaserTruthEventContainer.h"
#include "xAODFaserTruth/FaserTruthEventAuxContainer.h"
#include "xAODFaserTruth/xAODFaserTruthHelpers.h"

namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODFASERTRUTH {
      // The DataVector types:
      xAOD::FaserTruthParticleContainer    c1;
      xAOD::FaserTruthVertexContainer      c2;
      xAOD::FaserTruthEventContainer       c3;

      // The smart pointer types:
      DataLink< xAOD::FaserTruthParticleContainer > dl1;
      std::vector< DataLink< xAOD::FaserTruthParticleContainer > > dl2;
      DataLink< xAOD::FaserTruthVertexContainer > dl3;
      std::vector< DataLink< xAOD::FaserTruthVertexContainer > > dl4;
      ElementLink< xAOD::FaserTruthParticleContainer > el1;
      std::vector< ElementLink< xAOD::FaserTruthParticleContainer > > el2;
      std::vector< std::vector< ElementLink< xAOD::FaserTruthParticleContainer > > > el3;
      ElementLink< xAOD::FaserTruthVertexContainer > el4;
      std::vector< ElementLink< xAOD::FaserTruthVertexContainer > > el5;
      std::vector< std::vector< ElementLink< xAOD::FaserTruthVertexContainer > > > el6;
   };
}

#endif // XAODFASERTRUTH_XAODFASERTRUTHDICT_H
