// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: TrackSummaryAccessors_v1.h 574227 2013-12-06 14:20:39Z emoyse $
#ifndef XAOD_FASERTRACKSUMMARYACCESSORS_H
#define XAOD_FASERTRACKSUMMARYACCESSORS_H

// EDM include(s):
#include "AthContainers/AuxElement.h"

// Local include(s):
#include "xAODFaserTracking/FaserTrackingPrimitives.h"

namespace xAOD {

   /// Helper function for managing TrackSummary Accessor objects
   ///
   /// This function holds on to Accessor objects that can be used by each
   /// TrackParticle_v1 object at runtime to get/set summary values on themselves.
   ///
   template <class T>
   SG::AuxElement::Accessor< T >*
   faserTrackSummaryAccessor( xAOD::FaserSummaryType type );

} // namespace xAOD

#endif // XAOD_FASERTRACKSUMMARYACCESSORS_H
