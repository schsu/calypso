#ifndef XAODFASERTRACKING_TRACKAUXCONTAINER_H
#define XAODFASERTRACKING_TRACKAUXCONTAINER_H
 
// System include(s):
extern "C" {
#   include <stdint.h>
}
#include <vector>

// EDM include(s):
#include "xAODCore/AuxContainerBase.h"
#include "AthLinks/ElementLink.h"

// Local include(s):
#include "xAODFaserTracking/Track.h"
 
namespace xAOD {
    class TrackAuxContainer : public AuxContainerBase {
    
    public:
    /// Default constructor
    TrackAuxContainer();
    /// Dumps contents (for debugging)
    void dump() const;
    
    private:
    
    /// @name Defining parameters (perigee)
    /// @{
    std::vector< float >                x0;
    std::vector< float >                y0;
    std::vector< float >                phi0;
    std::vector< float >                theta;
    std::vector< float >                qOverP;
    
    std::vector< std::vector<float> >   definingParametersCovMatrix;
    
    std::vector< float >                vx;
    std::vector< float >                vy;
    std::vector< float >                vz;
    /// @}
    
    // /// @name Parameters
        /// We store the 3-pos, 3-mom and charge, and on the transient side these will be transformed into curvilinear parameters.
        /// Also stored are the cov matrix (still expressed in local coordinate frame) and parameter position.
        /// @{
        std::vector< std::vector<float> >   parameterX;
        std::vector< std::vector<float> >   parameterY;
        std::vector< std::vector<float> >   parameterZ;
        std::vector< std::vector<float> >   parameterPX;
        std::vector< std::vector<float> >   parameterPY;
        std::vector< std::vector<float> >   parameterPZ;

        std::vector< std::vector<float> >   trackParameterCovarianceMatrices;
        std::vector< std::vector<uint8_t> > parameterPosition;
    
    std::vector< xAOD::Track::StripClusterLinks_t > clusterLinks;
    std::vector< uint32_t > hitPattern;
    
    /// @name Fit quality functions
    /// @{
    std::vector< float >                chiSquared;
    std::vector< float >                numberDoF;
    /// @}
    
    /// @name TrackInfo functions
    /// @{
    std::vector< uint8_t >              trackFitter;
    std::vector< uint8_t >              particleHypothesis;
    /// @}
    
    #ifndef XAODFASERTRACKING_SUMMARYDYNAMIC
    /// @name TrackSummary information
    /// @{
    std::vector< uint8_t >         numberOfContribStripLayers       ;
    std::vector< uint8_t >         numberOfStripHits                  ;
    std::vector< uint8_t >         numberOfStripOutliers              ;
    std::vector< uint8_t >         numberOfStripHoles                 ;
    std::vector< uint8_t >         numberOfStripDoubleHoles           ;
    std::vector< uint8_t >         numberOfStripSharedHits            ;
    std::vector< uint8_t >         numberOfStripDeadSensors           ;
    std::vector< uint8_t >         numberOfStripSpoiltHits            ;
    
    std::vector< uint8_t >         numberOfOutliersOnTrack          ;
    std::vector< uint8_t >         standardDeviationOfChi2OS        ;
    
    /// @}
    #endif
    }; // class TrackAuxContainer
}

// Set up a CLID and StoreGate inheritance for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackAuxContainer, 1209585198, 1 )


#endif // XAODFASERTRACKING_TRACKAUXCONTAINER_H