#ifndef XAODFASERTRACKING_STRIPRAWDATAAUXCONTAINER_H
#define XAODFASERTRACKING_STRIPRAWDATAAUXCONTAINER_H

// System include(s):
#include <vector>

// Core include(s):
#include "xAODCore/AuxContainerBase.h"
#include "AthLinks/ElementLink.h"
 
namespace xAOD {
    class StripRawDataAuxContainer : public AuxContainerBase {
    public:
        StripRawDataAuxContainer();
        /// Dumps contents (for debugging)
        void dump() const;
    private:
        std::vector<uint64_t> id;
        std::vector<uint32_t> dataword;
    };
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::StripRawDataAuxContainer , 1173573595 , 1 )
 
#endif // XAODFASERTRACKING_STRIPRAWDATAAUXCONTAINER_H